from google.appengine._internal.django.template.defaultfilters import linebreaksbr
from google.appengine.ext.webapp import template
import re

__author__ = 'shwetanka'

register = template.create_template_register()

@register.filter('clean_html')
def clean_html(value):
    if '<pre><code>' not in value:
        return linebreaksbr(value)

    new_val = value.replace('<pre><code>', '_S_').replace('</code></pre>', '_E_')
    new_val = new_val.replace('<', '&lt;').replace('>', '&gt;')
    new_val = new_val.replace('_S_', '<pre><code>').replace('_E_', '</code></pre>')
    text_list = re.split('<pre><code>|</code></pre>', new_val)
    for i in range(0, len(text_list)-1):
        if i%2 == 0:
            text_list[i] = text_list[i].replace('\n', '<br>')
    res = ''
    for i in range(0, len(text_list)-1):
        if i%2==0:
            res = res+text_list[i]+'<pre><code>'
        else:
            res = res+text_list[i]+'</code></pre>'

    return res #todo: Add links
