import os
from google.appengine.ext.webapp import template
from webapp2 import uri_for
import webapp2
from db import auth, manager
from db.models import Ref

__author__ = 'shwetanka'


class RefHandler(webapp2.RequestHandler):
    def get(self, ref_id=None):
        cur_user = auth.cur_user_in_session()
        if not cur_user:
            self.redirect_to('home')

        if ref_id:
            try:
                ref_id = int(ref_id)
            except ValueError:
                return self.response.set_status(404, 'Not Found')

            ref = manager.get_ref_by_id(ref_id)
            if not ref:
                return self.response.set_status(404, 'Not Found')
            elif not ref.can_access(cur_user):
                return self.response.set_status(401, 'Forbidden')
            path = os.path.join(os.path.dirname(__file__), 'templates/view_ref.html')
            self.response.out.write(template.render(path, {'ref': ref, 'cur_user': cur_user,
                                                           'all_tags': manager.get_tags(cur_user)}))
        else:
            return self.response.set_status(404, 'Not Found')


class RefCrudHandler(webapp2.RequestHandler):
    def get(self):
        cur_user = auth.cur_user_in_session()
        if not cur_user:
            self.redirect_to('home')

        values = {'cur_user': cur_user, 'logout_url': uri_for('logout'), 'ref_post_url': uri_for('ref_edit'),
                  'all_tags': manager.get_tags(cur_user)}
        ref_id = self.request.GET.get('id', None)

        if ref_id:
            try:
                ref_id = int(ref_id)
            except ValueError:
                return self.response.set_status(404, 'Not Found')

            ref = manager.get_ref_by_id(ref_id)
            if not ref:
                return self.response.set_status(404, 'Not Found')
            elif not ref.can_access(cur_user):
                return self.response.set_status(401, 'Forbidden')

            values['ref'] = ref
            values['form_label'] = 'Edit'
        else:
            values['ref'] = Ref(title='', content='')
            values['form_label'] = 'Add'

        path = os.path.join(os.path.dirname(__file__), 'templates/edit_ref.html')
        self.response.out.write(template.render(path, values))

    def post(self):
        cur_user = auth.cur_user_in_session()
        if not cur_user:
            self.redirect_to('home')

        values = {'cur_user': cur_user, 'logout_url': uri_for('logout'), 'ref_post_url': uri_for('ref_edit')}
        ref_id = self.request.POST.get('id', None)

        is_valid = True
        content = self.request.POST.get('content', None)
        if not content:
            is_valid = False
            values['error'] = {'content': 'Ref content is required!'}

        tags = self.request.POST.getall('tags')

        if ref_id:
            try:
                ref_id = int(ref_id)
            except ValueError:
                return self.response.set_status(404, 'Not Found')
            ref = manager.get_ref_by_id(ref_id)
            if not ref:
                return self.response.set_status(404, 'Not Found')

            values['ref'] = ref
            values['all_tags'] = manager.get_tags(cur_user)
            if not is_valid:
                path = os.path.join(os.path.dirname(__file__), 'templates/edit_ref.html')
                return self.response.out.write(template.render(path, values))

            manager.update_ref(ref, content, cur_user, self.request.POST.get('title', None), tags=tags)
            self.redirect_to('ref_list')
        else:
            content = self.request.POST.get('content', None)
            if not is_valid:
                values['ref'] = Ref(content='', title='')
                values['all_tags'] = manager.get_tags(cur_user)
                path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
                return self.response.out.write(template.render(path, values))

            manager.create_ref(content, cur_user, self.request.POST.get('title', None), tags=tags)
            self.redirect_to('ref_list')


class RefListHandler(webapp2.RequestHandler):
    def get(self):
        cur_user = auth.cur_user_in_session()
        if not cur_user:
            self.redirect_to('home')

        values = {'cur_user': cur_user, 'logout_url': uri_for('logout')}
        tag_id = self.request.GET.get('tag', None)
        tag = None
        if tag_id:
            try:
                tag_id = int(tag_id)
            except ValueError:
                return self.response.set_status(404, 'Not Found')

            tag = manager.get_tag_by_id(tag_id)
            if not tag:
                return self.response.set_status(404, 'Not Found')

        refs = manager.refs_list(cur_user, tag)
        values['refs'] = refs
        values['all_tags'] = manager.get_tags(cur_user)
        if tag:
            values['cur_tag'] = tag

        path = os.path.join(os.path.dirname(__file__), 'templates/ref_list.html')
        self.response.out.write(template.render(path, values))
