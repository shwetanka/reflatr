import datetime
from db.models import User, Social
from gaesessions import get_current_session
from util import CUR_USER

__author__ = 'shwetanka'

def cur_user_in_session():
    session = get_current_session()
    if session.is_active():
        return session.get(CUR_USER, None)
    return None


def login(session, cur_user):
    session.regenerate_id()
    session.start()
    session.set_quick(CUR_USER, cur_user)

def signup(email, name=None, admin=False):
    user = User(email=email)
    user.name = name
    user.username = email.split("@")[0]
    user.admin = admin
    #user.last_login = date
    user.put()
    return user

def user_last_login(user):
    user.last_login = datetime.datetime.now()
    user.put()
    return user

def create_user(g_user, admin=False):
    user = signup(g_user.email(), name=g_user.nickname(), admin=admin)
    create_social(user, 'GM', g_user.user_id())
    return user

def get_or_create_user(g_user, admin=False):
    user = User.all().filter("email =", g_user.email()).get()
    if user:
        exist = False
        social_set = user.social_set
        for social in social_set:
            if social.source == 'GM':
                exist = True
        if not exist:
            create_social(user, 'GM', g_user.user_id())

        return (False, user)
    else:
        return (True, create_user(g_user, admin=admin))

def create_social(user, source, source_id, access_token=None):
    social = Social(user=user, source=source, source_id=source_id, access_token=access_token)
    social.put()
    return social

def create_fb_user(fb_user):
    user = signup(fb_user['email'], name=fb_user['name'])
    create_social(user, source='FB', source_id=fb_user['id'], access_token=fb_user['access_token'])
    return user


def get_or_create_fb_user(fb_user):
    user = User.all().filter('email =', fb_user['email']).get()
    if user:
        exist = False
        social_set = user.social_set
        for social in social_set:
            if social.source == 'FB':
                exist = True
        if not exist:
            create_social(user, 'FB', fb_user['id'], access_token=fb_user['access_token'])

        return False, user
    else:
        return True, create_fb_user(fb_user)
