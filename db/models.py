from google.appengine.ext import db
from google.appengine.ext.db import NotSavedError

__author__ = 'shwetanka'

SOURCES = (u'FB', u'GM')


class User(db.Model):
    name = db.StringProperty(verbose_name='Name')
    email = db.EmailProperty(verbose_name='Email', required=True)
    username = db.StringProperty(verbose_name='User Name', required=False)
    created_on = db.DateTimeProperty(auto_now_add=True)
    updated_on = db.DateTimeProperty(auto_now=True)
    last_login = db.DateTimeProperty()
    admin = db.BooleanProperty(default=False)


class Social(db.Model):
    source = db.StringProperty(required=True, choices=SOURCES)
    source_id = db.StringProperty()
    access_token = db.StringProperty()
    user = db.ReferenceProperty(User)


class Ref(db.Model):
    title = db.StringProperty(verbose_name='Title')
    content = db.TextProperty()
    tags = db.StringListProperty()
    created_on = db.DateTimeProperty(auto_now_add=True)
    updated_on = db.DateTimeProperty(auto_now=True)
    user = db.ReferenceProperty(reference_class=User)

    def can_access(self, user):
        return self.user.key() == user.key()

    def ref_id(self):
        try:
            return self.key().id()
        except NotSavedError:
            return None

    def get_tags(self):
        return Tag.all().filter('name IN', self.tags)

class Tag(db.Model):
    name = db.StringProperty(required=True)
    user = db.ReferenceProperty(reference_class=User, required=True)
    deleted = db.BooleanProperty(default=False)

    def tag_id(self):
        return self.key().id()

NAMES = (u'U', u'R', u'T')

class ModelIds(db.Model):
    name = db.StringProperty(required=True, choices=NAMES)
    id_val = db.IntegerProperty(default=1, required=True)