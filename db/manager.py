from db.models import Ref, Tag

__author__ = 'shwetanka'

def create_ref(content, user, title=None, tags=None):
    ref = Ref(content=content, user=user)
    ref.title = title

    #save tags
    def get_tag(name):
        tg = Tag.all().filter('user =', user).filter('name =', name).get()
        if not tg:
            tg = Tag(name=name, user=user)
            tg.put()
        return tg

    user_tags = map(get_tag, tags)
    ref.tags = [t.name for t in user_tags]
    return ref.put()

def get_ref_by_id(ref_id):
    return Ref.get_by_id(ref_id)

def update_ref(ref, content, user, title=None, tags=None):
    ref.content = content
    ref.user = user
    ref.title = title

    def get_tag(name):
        tg = Tag.all().filter('user =', user).filter('name =', name).get()
        if not tg:
            tg = Tag(name=name, user=user)
            tg.put()
        return tg

    user_tags = map(get_tag, tags)
    ref.tags = [t.name for t in user_tags]
    ref.put()
    return ref

def refs_list(user, tag=None):
    refs = Ref.all().filter('user =', user)
    if tag:
        refs.filter('tags =', tag.name)
    return refs.fetch(100)

def get_tags(user):
    return Tag.all().filter('user =', user).fetch(100)

def get_tag_by_id(id):
    return Tag.get_by_id(id)