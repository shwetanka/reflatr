#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from google.appengine._internal.django.template import Template
from google.appengine.api import users
import webapp2
import os
from google.appengine.ext.webapp import template
from webapp2 import uri_for
from db import auth
import facebook
from gaesessions import get_current_session
from ref import RefHandler, RefCrudHandler, RefListHandler
from util import CUR_USER

template.register_template_library('templatetags.code_filter')


class MainHandler(webapp2.RequestHandler):
    def get(self):
        session = get_current_session()
        if session.is_active():
            cur_user = session.get(CUR_USER, None)
            if cur_user:
                self.redirect_to('ref_edit')

        path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
        self.response.out.write(template.render(path, {'google_login': uri_for('glogin')}))


class FbStartLogin(webapp2.RequestHandler):
    def get(self):
        session = get_current_session()
        if session.is_active():
            if session.get(CUR_USER, None):
                self.redirect_to('ref_edit')

        next = self.request.GET.get('next', self.request.headers.get('referrer', '/'))
        session.set_quick('next', next)
        self.redirect(facebook.auth_url())


class FacebookHandler(webapp2.RequestHandler):
    def get(self):
        session = get_current_session()
        if session.is_active():
            if session.get(CUR_USER, None):
                self.redirect_to('ref_edit')

        code = self.request.GET.get('code', None)
        error = self.request.GET.get('error', None)
        if code is None:
            if error and error == 'access_denied':
                pass #todo: show access denied message
            else:
                pass #todo: fb error message

            return self.redirect_to('home')

        access_token = facebook.get_access_token_from_code(code)
        api = facebook.GraphAPI(access_token['access_token'])
        fb_user = api.get_object("me")
        fb_user['access_token'] = access_token['access_token']
        created, cur_user = auth.get_or_create_fb_user(fb_user)
        next = session.get('next', '/')
        auth.login(session, cur_user)
        auth.user_last_login(cur_user)

        self.redirect(next)


class GoogleHandler(webapp2.RequestHandler):
    def get(self):
        session = get_current_session()
        if session.is_active():
            if session.get(CUR_USER, None):
                self.redirect_to('ref_edit')

        g_user = users.get_current_user()
        if g_user:
            created, cur_user = auth.get_or_create_user(g_user, users.is_current_user_admin())
            auth.login(session, cur_user)
            auth.user_last_login(cur_user)
            self.redirect_to('home')
        else:
            self.redirect(users.create_login_url('/auth/glogin'))


class LogoutHandler(webapp2.RequestHandler):
    def get(self):
        session = get_current_session()
        session.terminate()
        self.redirect_to('home')


class AboutHandler(webapp2.RedirectHandler):
    def get(self, *args, **kwargs):
        path = os.path.join(os.path.dirname(__file__), 'templates/about.html')
        self.response.out.write(template.render(path, {}))

app = webapp2.WSGIApplication([
    webapp2.Route('/', handler=MainHandler, name='home', methods=['GET']),
    webapp2.Route('/auth/glogin', handler=GoogleHandler, name='glogin'),
    webapp2.Route('/auth/fbl', handler=FbStartLogin, name='fbl'),
    webapp2.Route('/auth/fblogin', handler=FacebookHandler, name='fblogin'),
    webapp2.Route('/ref/edit', handler=RefCrudHandler, name='ref_edit'),
    webapp2.Route('/ref/<ref_id:\d+>', handler=RefHandler, name='view_ref'),
    webapp2.Route('/refs', handler=RefListHandler, name='ref_list'),
    webapp2.Route('/logout', handler=LogoutHandler, name='logout'),
    webapp2.Route('/about', handler=AboutHandler, name='about'),
], debug=True)
